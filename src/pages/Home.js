
import { useEffect, useState } from "react"
import { FormStudent } from "../components/FormStudent";
import { Student } from "../components/Student";
import { StudentService } from "../shared/student-service";



export function Home() {

    const [students, setStudents] = useState([]);

    async function fetchStudents() {
        const response = await StudentService.fetchAll();

        setStudents(response.data);
    }

    async function deleteStudent(id) {
        await StudentService.delete(id);
        setStudents(
            students.filter(item => item.id !== id)
        );
    }

    async function addStudent(student) {
        const response = await StudentService.add(student);
        setStudents([
            ...students,
            response.data
        ]);
    }


    useEffect(() => {
        fetchStudents();
    }, []);



    return (
        <div>

            <h1>Welcome to my student list</h1>

            <section className="row">
                {students.map(item =>
                    <Student key={item.id}
                        student={item}
                        onDelete={deleteStudent} />
                )}
            </section>
            <FormStudent onFormSubmit={addStudent} />
        </div>
    )
}