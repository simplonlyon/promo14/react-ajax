import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import { StudentService } from "../shared/student-service";



export function Detail() {

    const [student, setStudent] = useState(null);
    const { id } = useParams();

    useEffect(() => {

        async function fetch() {
            const response = await StudentService.fetchOne(id);
            setStudent(response.data);
        }
        fetch();
        // StudentService.fetchOne(id)
        //             .then(response => setStudent(response.data));
    }, [id]);
    
    if (student) {
        return (

            <div>
                <p>Id: {student.id}</p>
                <p>First Name: {student.firstName}</p>
                <p>Name: {student.name}</p>
                <p>Birth Date: {student.birthDate}</p>
            </div>
        )
    } else {
        return (

            <div>
                loading...
            </div>
        )
    }
}