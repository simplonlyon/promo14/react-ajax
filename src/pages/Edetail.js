//import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { FormStudent } from "../components/FormStudent";
import { StudentService } from "../shared/student-service";


export function Edetail() {
    const { id } = useParams();
    const [editing, setEditing] = useState(false);
    const [student, setStudent] = useState(null);

    useEffect(() => {
        async function fetch() {
            const response = await StudentService.fetchOne(id);
            setStudent(response.data);
        }
        fetch();
    }, [id]);

    function handleSubmit(formValue) {
        //Ici il faudrait faire une requête patch vers la bdd
        setEditing(false); 
        setStudent(formValue);
    }

    if (student) {

        return (
            <>
            {!editing && 
                <section>
                    <p>id: {student.id}</p>
                    <p>name: {student.name}</p>
                    <p>first Name : {student.firstName}</p>
                    <p>Birth Date : {student.birthDate}</p>
                    <button className="btn btn-primary" onClick={() => setEditing(true)}>Edit</button>
                </section>
                }

            {editing && 
                <FormStudent student={student} onFormSubmit={handleSubmit} />
            }
            </>
        );
    } else {
        return (
            <p>Loading...</p>
        )
    }
}
