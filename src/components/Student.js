import { Link } from "react-router-dom";



export function Student({ student, onDelete }) {

    return (
        <div className="col-3">
            <article className="card">
                <div className="card-body">
                    {student.firstName} {student.name}
                </div>
                <div className="card-footer text-end">
                    <Link className="btn btn-primary mx-2" to={"/detail/"+student.id}>View</Link>
                    <button className="btn btn-danger" onClick={() => onDelete(student.id)}>X</button>
                </div>
            </article>
        </div>
    );
}