import { useState } from "react";



const initialState = {
    name: '',
    firstName: '',
    birthDate: ''
}

export function FormStudent({onFormSubmit, student = initialState}) {

    const [form, setForm] = useState(student);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>First Name :</label>
            <input className="form-control" required type="text" name="firstName" onChange={handleChange} value={form.firstName} />
            <label>Name :</label>
            <input className="form-control" required type="text" name="name" onChange={handleChange} value={form.name} />
            <label>Birthdate :</label>
            <input className="form-control" type="date" name="birthDate" onChange={handleChange} value={form.birthDate} />
            <button className="btn btn-primary">Submit</button>
        </form>
    );
}