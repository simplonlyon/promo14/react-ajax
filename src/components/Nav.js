import {  NavLink } from "react-router-dom";



export function Nav() {
    return (

        <nav>
            <ul>
                <li>
                    <NavLink activeClassName="active" to="/">Home</NavLink>
                </li>
            </ul>
        </nav>
    )
}