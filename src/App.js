
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Home } from './pages/Home';
import { Nav } from './components/Nav';
import { Detail } from './pages/Detail';
//import { Edetail } from './pages/Edetail';

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="container-fluid">
        <Nav />
        
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/detail/:id"> 
            <Detail/>
          </Route>

        </Switch>
      </div>
    </Router>
  );
}

export default App;
