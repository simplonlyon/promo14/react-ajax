import axios from "axios";

export class StudentService {
    
    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/student');
    }

    static async delete(id) {
        axios.delete(process.env.REACT_APP_SERVER_URL+'/api/student/' + id);
    }

    static async add(student) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/student',student);
    }

    static async fetchOne(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/student/' + id);
    }
}